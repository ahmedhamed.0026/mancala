package com.mancala.app.service;

import com.mancala.app.domain.MancalaGameConverter;
import com.mancala.app.domain.MancalaGameDO;
import com.mancala.app.model.MancalaBoard;
import com.mancala.app.model.MancalaGame;
import com.mancala.app.model.PlayerTurn;
import com.mancala.app.repository.KeyValueStorage;
import com.mancala.app.rules.MancalaRules;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import org.junit.runner.RunWith;

import java.util.stream.IntStream;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class MancalaServiceTest {
    @Mock
    MancalaGameServiceImpl mancalaGameService;

    MancalaGameConverter converter;

    @Before
    public void setUp(){
        this.mancalaGameService = new MancalaGameServiceImpl();
        converter = new MancalaGameConverter();
    }

    @Test
    public void testCreateNewGame(){
        MancalaGameDO mancalaGameDO = new MancalaGameDO();
        mancalaGameDO.setGameId("1");
        mancalaGameDO.setPitId("2");
        MancalaGame game =
        mancalaGameService.strew(mancalaGameDO);
        assertNotNull(game);
    }

    @Test
    public void testGameOver(){
        MancalaGame mancalaGame = prepareMap();
        mancalaGame.setBoard(getBoard(false));
        mancalaGame.getBoard().getPit(MancalaRules.PLAYER_B_SIXTH_HOUSE_ID).setSeeds(1);
        mancalaGame.setPlayerTurn(PlayerTurn.PLAYER_B);
        KeyValueStorage.gameIds.put("1", mancalaGame);
        MancalaGameDO mancalaGameDO = new MancalaGameDO();
        mancalaGameDO.setGameId("1");
        mancalaGameDO.setPitId("12");
        mancalaGame.getBoard().getPits().get(MancalaRules.PLAYER_B_SCORE_HOUSE_ID).setSeeds(1);
        mancalaGame = mancalaGameService.strew(mancalaGameDO);
        System.out.println(mancalaGame.getId());
        System.out.println(mancalaGame.isOver());
        assertTrue(mancalaGame.isOver());
    }

    @Test
    public void testWhosTurn(){
        MancalaGameDO mancalaGameDO = new MancalaGameDO();
        mancalaGameDO.setGameId("1");
        mancalaGameDO.setPitId("12");
        MancalaGame mancalaGame =  mancalaGameService.strew(mancalaGameDO);
        assertEquals(PlayerTurn.PLAYER_A, mancalaGame.getPlayerTurn());
    }

    @Test
    public void whoWins(){
        MancalaGame mancalaGame = new MancalaGame("1");
        MancalaBoard board = new MancalaBoard();
        board.getPit(MancalaRules.PLAYER_A_THIRD_PIT_ID).setSeeds(MancalaRules.PLAYER_B_SIXTH_HOUSE_ID);
        mancalaGame.setBoard(board);
        mancalaGame.setOver(true);
        mancalaGame.setPlayerTurn(PlayerTurn.PLAYER_A);
        mancalaGame.getBoard().getPit(MancalaRules.PLAYER_B_SCORE_HOUSE_ID).setSeeds(14);
        mancalaGame.getBoard().getPit(MancalaRules.PLAYER_A_SCORE_HOUSE_ID).setSeeds(0);
        KeyValueStorage.gameIds.put("1",mancalaGame);
        MancalaGameDO mancalaGameDO =new MancalaGameDO();
        mancalaGameDO.setPitId("2");
        mancalaGameDO.setGameId("1");
        mancalaGame = mancalaGameService.strew(mancalaGameDO);
        assertEquals(PlayerTurn.PLAYER_B, mancalaGame.getWinner());
    }

    @Test
    public void strewWithStealBonus(){
        MancalaGame mancalaGame = new MancalaGame("1");
        MancalaBoard board = getBoard(true);
        board.getPit(MancalaRules.PLAYER_B_THIRD_PIT_ID).setSeeds(1);
        board.getPit(MancalaRules.PLAYER_B_FORTH_HOUSE_ID).clear();
        board.getPit(MancalaRules.PLAYER_A_THIRD_PIT_ID).setSeeds(4);
        mancalaGame.setBoard(board);
        mancalaGame.setOver(false);
        mancalaGame.setPlayerTurn(PlayerTurn.PLAYER_B);
        KeyValueStorage.gameIds.put("1",mancalaGame);
        MancalaGameDO mancalaGameDO =new MancalaGameDO();
        mancalaGameDO.setPitId("9");
        mancalaGameDO.setGameId("1");
        mancalaGame = mancalaGameService.strew(mancalaGameDO);
        assertEquals(11,mancalaGame.getBoard().getPit(MancalaRules.PLAYER_B_SCORE_HOUSE_ID).getSeeds());
    }
    @Test
    public void strewWithDoubleTurn(){
        MancalaGame mancalaGame = new MancalaGame("1");
        MancalaBoard board = getBoard(true);
        board.getPit(MancalaRules.PLAYER_B_SIXTH_HOUSE_ID).setSeeds(1);
        mancalaGame.setBoard(board);
        mancalaGame.setOver(false);
        mancalaGame.setPlayerTurn(PlayerTurn.PLAYER_B);
        KeyValueStorage.gameIds.put("1",mancalaGame);
        MancalaGameDO mancalaGameDO =new MancalaGameDO();
        mancalaGameDO.setPitId("12");
        mancalaGameDO.setGameId("1");
        mancalaGame = mancalaGameService.strew(mancalaGameDO);
        assertEquals(PlayerTurn.PLAYER_B,mancalaGame.getPlayerTurn());
    }
    private MancalaGame prepareMap(){
        MancalaGame mancalaGame = new MancalaGame("1");
        MancalaBoard board = new MancalaBoard();
        mancalaGame.setBoard(board);
        return mancalaGame;
    }
    private MancalaBoard getBoard(boolean full){
        MancalaBoard mancalaBoard = new MancalaBoard();
        if (full){
            IntStream.rangeClosed(MancalaRules.PLAYER_A_FIRST_PIT_ID,
                    MancalaRules.PLAYER_B_SCORE_HOUSE_ID).forEach(index -> mancalaBoard.getPit(index).setSeeds(6));
        }else{
            IntStream.rangeClosed(MancalaRules.PLAYER_A_FIRST_PIT_ID,MancalaRules.PLAYER_B_SCORE_HOUSE_ID).forEach(index -> mancalaBoard.getPit(index).setSeeds(0));
        }
        return  mancalaBoard;
    }
}
