package com.mancala.app.domain;

import com.mancala.app.model.MancalaGame;
import com.mancala.app.service.MancalaGameService;
import com.mancala.app.service.MancalaGameServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class MancalaGameConverterTest {

    private MancalaGameConverter converter;
    private MancalaGameService mancalaGameService;

    @Before
    public void setUp(){
        mancalaGameService = new MancalaGameServiceImpl();
        converter = new MancalaGameConverter();
    }
    @Test
    public void testConverter(){
        MancalaGameDO mancalaGameDO = new MancalaGameDO();
        mancalaGameDO.setPitId("1");
        mancalaGameDO.setGameId("1");
        MancalaGame game  = mancalaGameService.strew(mancalaGameDO);
        MancalaGameVO mancalaGameVO = converter.convertGameToMap(game);
        Assert.assertEquals(mancalaGameVO.getGameMap().get(0),Integer.valueOf(6));
        Assert.assertEquals(mancalaGameVO.getGameMap().get(1),Integer.valueOf(0));

    }
}
