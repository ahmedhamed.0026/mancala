package com.mancala.app.domain;

import com.mancala.app.model.MancalaBoard;
import com.mancala.app.model.MancalaGame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class MancalaGameConverter {

    Logger logger =  LoggerFactory.getLogger(MancalaGameConverter.class);
    public MancalaGameVO convertGameToMap(MancalaGame game){
        logger.info("convertGameToMap ..");
        Map<Integer,Integer> gameMap = new HashMap<>();
        MancalaBoard board = game.getBoard();
        board.getPits().forEach(pit -> gameMap.put(pit.getPitId(),pit.getSeeds()));
        return new MancalaGameVO(game.getId(),gameMap);
    }
}
