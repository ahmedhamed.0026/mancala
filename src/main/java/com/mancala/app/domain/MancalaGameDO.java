package com.mancala.app.domain;



public class MancalaGameDO {

    private String gameId ;
    private String pitId ;

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getPitId() {
        return pitId;
    }

    public void setPitId(String pitId) {
        this.pitId = pitId;
    }
}
