package com.mancala.app.domain;

import java.util.Map;

public class MancalaGameVO {
    private String gameId;
    private Map<Integer,Integer> gameMap ;

    public MancalaGameVO(String gameId, Map<Integer, Integer> gameMap) {
        this.gameId = gameId;
        this.gameMap = gameMap;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public Map<Integer, Integer> getGameMap() {
        return gameMap;
    }

    public void setGameMap(Map<Integer, Integer> gameMap) {
        this.gameMap = gameMap;
    }

    @Override
    public String toString() {
        return "MancalaGameVO{" +
                "gameId='" + gameId + '\'' +
                ", gameMapValues=" + gameMap.values()  +
                ", gameMapKeys=" + gameMap.keySet()  +
                '}';
    }
}
