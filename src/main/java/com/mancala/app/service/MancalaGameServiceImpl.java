package com.mancala.app.service;

import com.mancala.app.domain.MancalaGameDO;
import com.mancala.app.model.MancalaBoard;
import com.mancala.app.model.MancalaGame;
import com.mancala.app.model.Pit;
import com.mancala.app.model.PlayerTurn;
import com.mancala.app.repository.KeyValueStorage;
import com.mancala.app.rules.MancalaRules;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.util.Map;

@Service
public class MancalaGameServiceImpl implements  MancalaGameService{
   private Logger logger = LoggerFactory.getLogger(MancalaGameServiceImpl.class);
    private static final int EMPTY_SEEDS = 0;
    private static final int LAST_STONE = 1;

    private Map<String, MancalaGame> gameIds = KeyValueStorage.gameIds;

    @Override
    public MancalaGame strew(MancalaGameDO mancalaGameDO){
        logger.info("Mancala Game Service ... ");
        int pitId = Integer.parseInt(mancalaGameDO.getPitId());
        MancalaGame game = getGameById(mancalaGameDO.getGameId());
        if (game.isOver()){
            game.setWinner(whoWins(game));
            return game;
        }
        MancalaBoard board = game.getBoard();
        Pit pit = board.getPit(pitId);

        //return game if the pit is empty
        if (board.getPit(pit.getPitId()).getSeeds()<=EMPTY_SEEDS){
            return game;
        }


        // return game if its wrong player turn
        if (game.getPlayerTurn() == null)
        {
            game.setPlayerTurn(whosTurn(pitId));
        }else {
            if (!isRightTurn(game.getPlayerTurn(),pitId)){
                return  game;
            }
        }
        // check if the last stone
        strewSeeds(pit,game,pit.getSeeds()==LAST_STONE);
        game.setOver(isGameOver(game));
        return game;
    }
     MancalaGame getGameById(String gameId){
        if (gameIds.isEmpty()||gameIds.get(gameId)==null) {
            MancalaGame mancalaGame = new MancalaGame(gameId);
            MancalaBoard board = new MancalaBoard();
            mancalaGame.setBoard(board);
            gameIds.put(gameId,mancalaGame);
            return mancalaGame;
        }
        return gameIds.get(gameId);
    }
     void strewSeeds(Pit pit,MancalaGame game, boolean lastStone){
        int currentPitIndex= 0;
        if (lastStone && game.getBoard().getPit(pit.getPitId()+1).getSeeds()==0
                && game.getBoard().getPit((MancalaRules.PLAYING_PITS - pit.getPitId()+1)).getSeeds()>0){
            int allSeeds = 1 + game.getBoard().getPit(12-(pit.getPitId()+1)).getSeeds() ;
            if (game.getPlayerTurn()== PlayerTurn.PLAYER_A){
                game.getBoard().getPit(MancalaRules.PLAYER_A_SCORE_HOUSE_ID).
                        setSeeds(game.getBoard().getPit(6).getSeeds()+allSeeds);
            }else {
                game.getBoard().getPit(MancalaRules.PLAYER_B_SCORE_HOUSE_ID).
                        setSeeds(game.getBoard().getPit(13).getSeeds()+allSeeds);

            }
            game.getBoard().getPit(pit.getPitId()).clear();
            game.getBoard().getPit(12-(pit.getPitId()+1)).clear();
            switchGameTurn(game);
            return;
        }
        for (int i =0;i<game.getBoard().getPit(pit.getPitId()).getSeeds();i++){
            currentPitIndex = (pit.getPitId()+i)%MancalaRules.ALL_PITS ;
            game.getBoard().getPit(currentPitIndex).stew();
        }
        game.getBoard().getPit(pit.getPitId()).clear();

        // check if the current pit index is one of the pit Houses, if so the player should play again
        if (currentPitIndex==MancalaRules.PLAYER_A_SCORE_HOUSE_ID ||
          currentPitIndex == MancalaRules.PLAYER_B_SCORE_HOUSE_ID){
            return;
        }

            switchGameTurn(game);
    }
     PlayerTurn whosTurn(int pitId){
        if (pitId >= MancalaRules.PLAYER_A_FIRST_PIT_ID &&
                pitId<=MancalaRules.PLAYER_A_SIXTH_HOUSE_ID)
            return PlayerTurn.PLAYER_A;
        return PlayerTurn.PLAYER_B;
    }
     boolean isRightTurn(PlayerTurn playerTurn,int pitId){
        return whosTurn(pitId) == playerTurn ;
    }
   private void switchGameTurn(MancalaGame game){
        if (game.getPlayerTurn()==PlayerTurn.PLAYER_A)
        {
            game.setPlayerTurn(PlayerTurn.PLAYER_B);
        }else{
            game.setPlayerTurn(PlayerTurn.PLAYER_A);
        }

    }
   private boolean isGameOver(MancalaGame game){
        int playerAPitsSeeds = 0;
        int playerBPitsSeeds = 0;
        for (int i = 0;i<=5; i++){
            playerAPitsSeeds = playerAPitsSeeds+
                    game.getBoard().getPit(i).getSeeds() ;
        }
        for (int i = 7;i<=12; i++){
            playerBPitsSeeds = playerBPitsSeeds+
                    game.getBoard().getPit(i).getSeeds() ;
        }
        return playerAPitsSeeds == 0 || playerBPitsSeeds == 0;
    }
   PlayerTurn whoWins(MancalaGame game){
        if (game.getBoard().getPit(MancalaRules.PLAYER_A_SCORE_HOUSE_ID).getSeeds()>
                game.getBoard().getPit(MancalaRules.PLAYER_B_SCORE_HOUSE_ID).getSeeds())
            return PlayerTurn.PLAYER_A;
        return PlayerTurn.PLAYER_B;
    }
}


