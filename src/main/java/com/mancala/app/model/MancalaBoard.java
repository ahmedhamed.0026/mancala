package com.mancala.app.model;

import com.mancala.app.rules.MancalaRules;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class MancalaBoard {
    private static final int PITS_NUMBER = 14  ;
    private static final int DEFAULT_SEEDS = 6  ;


    private List<Pit> pits ;

    public MancalaBoard(){
        pits = new ArrayList<>();
        IntStream.rangeClosed(0,PITS_NUMBER).forEach(index ->
                pits.add(new Pit(index,DEFAULT_SEEDS)));
        pits.get(MancalaRules.PLAYER_A_SCORE_HOUSE_ID).clear();
        pits.get(MancalaRules.PLAYER_B_SCORE_HOUSE_ID).clear();
    }

    public Pit getPit(int id){
        return pits.stream().filter(p -> p.getPitId() == id).findAny().get() ;
    }
    public List<Pit> getPits() {
        return pits;
    }

    public void setPits(List<Pit> pits) {
        this.pits = pits;
    }
}
