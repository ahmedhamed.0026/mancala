package com.mancala.app.model;

public class Pit {
    private int pitId;
    
    private int seeds ;

    public Pit(int index, int value) {
        this.pitId = index;
        this.seeds = value;
    }

    public void clear(){
        this.setSeeds(0);
    }

    public void stew(){
        this.seeds ++ ;
    }
    public int getPitId() {
        return pitId;
    }

    public void setPitId(int pitId) {
        this.pitId = pitId;
    }

    public int getSeeds() {
        return seeds;
    }

    public void setSeeds(int seeds) {
        this.seeds = seeds;
    }

    @Override
    public String toString() {
        return "Pit{" +
                "pitId=" + pitId +
                ", seeds=" + seeds +
                '}';
    }
}
