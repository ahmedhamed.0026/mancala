package com.mancala.app.model;

import org.springframework.data.annotation.Id;

import java.io.Serializable;

public class MancalaGame implements Serializable {


    @Id
    private String id = "";
    private int idCounter = 0 ;
    private Player player_1;
    private Player player_2;
    private PlayerTurn playerTurn;
    private PlayerTurn winner;
    private boolean isOver;
    private MancalaBoard board;
    public String getId() {
        return id;
    }
   public MancalaGame(String id){
        this.idCounter ++ ;
        this.id = id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public Player getPlayer_1() {
        return player_1;
    }

    public void setPlayer_1(Player player_1) {
        this.player_1 = player_1;
    }

    public Player getPlayer_2() {
        return player_2;
    }

    public void setPlayer_2(Player player_2) {
        this.player_2 = player_2;
    }

    public boolean isOver() {
        return isOver;
    }

    public void setOver(boolean over) {
        isOver = over;
    }

    public MancalaBoard getBoard() {
        return board;
    }

    public void setBoard(MancalaBoard board) {
        this.board = board;
    }

    public int getIdCounter() {
        return idCounter;
    }

    public void setIdCounter(int idCounter) {
        this.idCounter = idCounter;
    }

    public PlayerTurn getPlayerTurn() {
        return playerTurn;
    }

    public void setPlayerTurn(PlayerTurn playerTurn) {
        this.playerTurn = playerTurn;
    }

    public PlayerTurn getWinner() {
        return winner;
    }

    public void setWinner(PlayerTurn winner) {
        this.winner = winner;
    }
}
