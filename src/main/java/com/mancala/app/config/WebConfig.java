package com.mancala.app.config;

public class WebConfig {

    public static final String MAPPING_ROUTE = "/mancala/v1" ;
    public static final String SUBSCRIBER_PATH = "/sub-play";

    private WebConfig(){

    }
}
