package com.mancala.app.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.*;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketCong implements WebSocketMessageBrokerConfigurer {
    static final String WS_PATH ="/create-game" ;
    static final  String ALLOWED_ORIGINS_LOCAL = "http://localhost:4200" ;
    static final String ALLOWED_ORIGIN_AWS = "http://mancala-ui.s3-website.us-east-2.amazonaws.com";

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config){
    config.enableSimpleBroker(WebConfig.SUBSCRIBER_PATH);
    config.setApplicationDestinationPrefixes("/");
    }
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint(WS_PATH).setAllowedOrigins(ALLOWED_ORIGIN_AWS,ALLOWED_ORIGINS_LOCAL)
                .withSockJS();

    }
}
