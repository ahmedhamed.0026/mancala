package com.mancala.app.controller;


import com.mancala.app.config.WebConfig;
import com.mancala.app.domain.MancalaGameConverter;
import com.mancala.app.domain.MancalaGameDO;
import com.mancala.app.domain.MancalaGameVO;
import com.mancala.app.service.MancalaGameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
public class GameController {
    private Logger logger = LoggerFactory.getLogger(GameController.class);

    @Autowired
    MancalaGameService service;
    private final SimpMessagingTemplate messagingTemplate;

    @Autowired
    GameController(SimpMessagingTemplate template) {
        this.messagingTemplate = template;
    }


    @MessageMapping("/play")
    @SendTo("/play")
    public void
    play(@RequestBody MancalaGameDO mancalaGameDO)  {
        logger.info("WebSocketController play() ...");
        MancalaGameVO mancalaGameVO= new MancalaGameConverter().convertGameToMap(service.strew(mancalaGameDO));
        this.messagingTemplate.convertAndSend(WebConfig.SUBSCRIBER_PATH,mancalaGameVO);
    }

}