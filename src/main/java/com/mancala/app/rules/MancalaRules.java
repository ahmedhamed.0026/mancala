package com.mancala.app.rules;

public class MancalaRules {
    public static final int ALL_PITS = 14;
    public static final int PLAYING_PITS = 12;

    public static final int PLAYER_A_SCORE_HOUSE_ID = 6 ;
    public static final int PLAYER_B_SCORE_HOUSE_ID = 13 ;

    public static final int PLAYER_A_FIRST_PIT_ID = 0 ;
    public static final int PLAYER_A_SECOND_PIT_ID = 1 ;
    public static final int PLAYER_A_THIRD_PIT_ID = 2 ;
    public static final int PLAYER_A_FORTH_HOUSE_ID = 3 ;
    public static final int PLAYER_A_FIFTH_HOUSE_ID = 4 ;
    public static final int PLAYER_A_SIXTH_HOUSE_ID = 5 ;

    public static final int PLAYER_B_FIRST_PIT_ID = 7 ;
    public static final int PLAYER_B_SECOND_PIT_ID = 8 ;
    public static final int PLAYER_B_THIRD_PIT_ID = 9 ;
    public static final int PLAYER_B_FORTH_HOUSE_ID = 10 ;
    public static final int PLAYER_B_FIFTH_HOUSE_ID = 11 ;
    public static final int PLAYER_B_SIXTH_HOUSE_ID = 12 ;

    private MancalaRules(){

    }

}
