package com.mancala.app.repository;

import com.mancala.app.model.MancalaGame;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MancalaGameRepository extends MongoRepository<MancalaGame,String>
{

}
