Mancala 
Backend implementation of Mancala game

Technologies
* Spring Boot
* Spring WebSockets
* STOMP
* Sonar Cube

To-Do / Issues
1 - Authentication
2 - Add Exception Handling 
3 - Add Integration Test cases
4 - Create a room so that the user can invites other player to play with acode 
5 - add unit test piple line 

How to run in local
To build the application : ./mvnw clean install
To start the application : mancala-0.0.1-SNAPSHOT.jar,  http://localhost:8080/mancala/

Deployment 
Deployed on AWS EC2t.
url: http://3.137.168.116:8080/mancala/

